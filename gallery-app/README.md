# GalleryApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.


## Development server
1. Run npm install from the 'gallery-app' sub directory under where you ran 'git clone' from. This is the directory that contains package.json
2. Then run the build command detailed further below (which is 'ng build')
3. Then run then either of these:
Run `npm run start` from root directory (where package.json resides) 

OR this does what above does (proxy is needed to avoid cross domain ajaxy browser errors):

Run `ng serve --port 4000 --proxy-config ./proxy.conf.json` for a dev server. Navigate to `http://localhost:4000/`. The app will automatically reload if you change any of the source files.
4. You should see this URL working in your browser: http://localhost:4000
5. After clicking a product in the gallery - try these keys which are kind of nice: RIGHT ARROW, LEFT ARROW, ESCAPE, ENTER. 
## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run from another terminal this command: `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Known issues: 2 of the 6 tests currently fail. Needs more setup for the Components as per: https://medium.com/@AikoPath/testing-angular-components-with-input-3bd6c07cfaf6

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
