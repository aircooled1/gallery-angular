import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  // below works
  // webServiceEndPointUrl = 'https://www.westelm.com/services/catalog/v4/category/shop/new/all-new/index.json';
  // USING PROXY TO GET AROUND CARS xmlhttprequest violation. Do not use this package.json (and proxy.conf.json) for production usage. Just dev purposes.
  // browswer calls localhost for the xmlhttprequest... and server forwards that onto the real server. Common fix for CARS violation.
  private webServiceEndPointUrl = '/services/catalog/v4/category/shop/new/all-new/index.json'; // see proxy.conf.json for forwarding to westelm server.

  constructor(private http: HttpClient) { }

  /** GET products from the server */
  getProducts (): Observable<any[]> {
    return this.http.get<any[]>(this.webServiceEndPointUrl)
  }

}
