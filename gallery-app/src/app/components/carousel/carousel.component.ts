import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37,
  ESCAPE = 27,
  ENTER = 13
}

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})

export class CarouselComponent implements OnInit {
  currentImageIndex: number;

  @Input() productElm: any;
  // @Output() hideCarousel: boolean;
  @Output() carouselCloseClicked: EventEmitter<any> = new EventEmitter<any>();
  constructor() { }

  @HostListener('window:keyup', ['$event'])
  keyEvent(event: KeyboardEvent) {
    console.log(event);
    
    if (event.keyCode === KEY_CODE.RIGHT_ARROW) {
      console.log('hey tell Modal to navigate Next');
      this.navigateThumbnail('next');
    }

    if (event.keyCode === KEY_CODE.LEFT_ARROW) {
      console.log('hey tell Modal to navigate Previous');
      this.navigateThumbnail('previous');
    }

    if (event.keyCode === KEY_CODE.ESCAPE || event.keyCode === KEY_CODE.ENTER ) {
      console.log('hey tell Modal to close');
      this.hideCarousel();
    }
  }

  hideCarousel() {
    this.carouselCloseClicked.emit();
  }

  navigateThumbnail(direction: string) {
    let lastIndex = this.productElm.images.length-1; // last valid index is length-1
    if (direction=='next') {
      if (this.currentImageIndex < lastIndex) {
        this.currentImageIndex++ ;
      } else {
        this.currentImageIndex = 0; // start cycle at 0 again.
      }
      
    } else if (direction=='previous') {
      if (this.currentImageIndex > 0) {
        this.currentImageIndex-- ;
      } else {
        this.currentImageIndex = lastIndex; // skip around to last index.
      }
    }
    console.log('currentImageIndex: '+ this.currentImageIndex);
  }

  ngOnInit() {
    this.currentImageIndex = 0;
  }

}
