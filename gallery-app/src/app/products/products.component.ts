import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../model/product';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})

export class ProductsComponent implements OnInit {
  showCarousel(productElm: any) {
    console.log('hey you clicked me');
    console.log(productElm.name);
    this.productWasClicked.emit(productElm);
  }
  @Input() productElm: any;
  constructor() {  }

  @Output()
  productWasClicked: EventEmitter<any> = new EventEmitter<any>();


  ngOnInit() {
  }

}
