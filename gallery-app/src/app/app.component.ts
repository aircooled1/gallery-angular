import { Component } from '@angular/core';
import { PRODUCTS, MOCK_ELM_PRODUCTS } from './data-mock/mock-products'
import { ProductService } from './product.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'A West Elm Product Gallery Viewer';
  products = PRODUCTS;
  mockElmProducts: any = MOCK_ELM_PRODUCTS;
  elmProducts: any;
  currentProduct: any;  // To Do: Not strongly typed yet. // = this.mockElmProducts[0];

  showCarousel: boolean = false;

  constructor(private productService: ProductService) { }

  ngOnInit() {
    
    // EITHER use this for hard coded data rather than ajax
    // this.elmProducts = this.mockElmProducts;

    // OR use call to ajaxy service below
    this.getProducts();
  }

  getProducts() {
    this.productService.getProducts().subscribe(products => {
      this.elmProducts = products['groups']; // groups node of products object is iterable product array.
      console.log('Inside app component getProducts - data back from server');
      console.log(this.elmProducts );
    });
  }

  handleProductWasClicked(productElm: any) {
    console.log('inside app component handleProductWasClicked');
    console.log(productElm.name);
    this.currentProduct = productElm; // set to clicked product
    this.showCarousel = true;
  }

  handleCarouselCloseClicked() {
    console.log('inside app component handleCarouselCloseClicked');
    this.showCarousel = false;
  }
}
